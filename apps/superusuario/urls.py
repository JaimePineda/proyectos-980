from django.conf.urls import url, include

from apps.superusuario.views import  Superusuario_delete, index_Superusuario, Superusuario_view, Superusuario_list, Superusuario_edit, Superusuario_viewAd, Superusuario_listAd, Superusuario_deleteAd, Superusuario_editAd

urlpatterns = [
    url(r'^$', index_Superusuario, name='index_Superusuario'),
    url(r'^nuevo$', Superusuario_view, name='Superusuario_crear'),
    url(r'^nuevoAd$', Superusuario_viewAd, name='Superusuario_crearAd'),
    url(r'^lista$', Superusuario_list, name='Superusuario_list'),
    url(r'^listaAd$', Superusuario_listAd, name='Superusuario_listAd'),
    url(r'^editar/(?P<id_superusuario>\d+)$', Superusuario_edit, name='Superusuario_edit'),
    url(r'^eliminar/(?P<id_superusuario>\d+)$', Superusuario_delete, name='Superusuario_deleteAd'),
    url(r'^editarAd/(?P<id_superusuario>\d+)$', Superusuario_editAd, name='Superusuario_edit'),
    url(r'^eliminarAd/(?P<id_superusuario>\d+)$', Superusuario_deleteAd, name='Superusuario_deleteAd'),
]