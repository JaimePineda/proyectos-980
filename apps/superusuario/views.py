from django.shortcuts import render, redirect
from django.http import HttpResponse

from apps.superusuario.forms import SuperusuarioForm
from apps.superusuario.models import usuarios

# Create your views here.
def index_Superusuario(request):
    return render(request, 'Superusuario/Index.html') 

def Superusuario_view(request):
    if request.method == 'POST':
        form = SuperusuarioForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = SuperusuarioForm()

    return render(request, 'Superusuario/Superusuario_form.html', {'form': form})

def Superusuario_list(request):
    superusuario = usuarios.objects.all().order_by('id')
    contexto = {'superusuario': superusuario}
    return render(request, 'Superusuario/Superusuario_list.html', contexto)


def Superusuario_edit(request, id_superusuario):
    superusuario = usuarios.objects.get(id=id_superusuario)
    if request.method == 'GET':
        form = SuperusuarioForm(instance=superusuario)
    else:
        form =SuperusuarioForm(request.POST, instance=superusuario)
        if form.is_valid():
            form.save()
    return render(request, 'Superusuario/Superusuario_form.html', {'form': form})

def Superusuario_delete(request, id_superusuario):
    superusuario = usuarios.objects.get(id=id_superusuario)
    if request.method == 'POST':
        superusuario.delete()
    return render(request, 'Superusuario/Superusuario_delete.html', {'superusuario': superusuario})





def Superusuario_viewAd(request):
    if request.method == 'POST':
        form = SuperusuarioForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = SuperusuarioForm()

    return render(request, 'Superusuario/Superusuario_formAd.html', {'form': form})

def Superusuario_listAd(request):
    superusuario = usuarios.objects.all().order_by('id')
    contexto = {'superusuario': superusuario}
    return render(request, 'Superusuario/Superusuario_listAd.html', contexto)


def Superusuario_editAd(request, id_superusuario):
    superusuario = usuarios.objects.get(id=id_superusuario)
    if request.method == 'GET':
        form = SuperusuarioForm(instance=superusuario)
    else:
        form =SuperusuarioForm(request.POST, instance=superusuario)
        if form.is_valid():
            form.save()
    return render(request, 'Superusuario/Superusuario_formAd.html', {'form': form})

def Superusuario_deleteAd(request, id_superusuario):
    superusuario = usuarios.objects.get(id=id_superusuario)
    if request.method == 'POST':
        superusuario.delete()
    return render(request, 'Superusuario/Superusuario_deleteAd.html', {'superusuario': superusuario})

