from django import forms

from apps.superusuario.models import usuarios

class SuperusuarioForm(forms.ModelForm):

    class Meta:
        model = usuarios

        fields = [
            'nombre_completo',
            'nombre_usuario',
            'correo',
            'telefono',
            'edad',
            'fotografia',
            'contrasenia',

        ]

        labels = {
            'nombre_completo': 'Nombre Completo',
            'nombre_usuario': 'Nombre Usuario',
            'correo': 'Correo',
            'telefono': 'Telefono',
            'edad': 'Edad',
            'fotografia': 'Fotografia',
            'contrasenia': 'Contrasenia',

        }

        widgets = {
            'nombre_completo': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_usuario': forms.TextInput(attrs={'class': 'form-control'}),
            'correo': forms.TextInput(attrs={'class': 'form-control'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.TextInput(attrs={'class': 'form-control'}),
            'fotografia': forms.TextInput(attrs={'class': 'form-control'}),
            'contrasenia': forms.TextInput(attrs={'class': 'form-control'}),
        }
