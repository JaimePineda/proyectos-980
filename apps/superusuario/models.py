from django.db import models

# Create your models here.
class usuarios(models.Model):
    nombre_completo = models.CharField(max_length=100)
    nombre_usuario = models.CharField(max_length=100)
    correo = models.EmailField()
    telefono = models.CharField(max_length=20)
    edad = models.IntegerField()
    fotografia = models.CharField(max_length=100)
    contrasenia = models.CharField(max_length=50)
