from django.shortcuts import render, redirect
from django.http import HttpResponse

from apps.Administrador.forms import AdministradorForm
from apps.Administrador.models import cliente
# Create your views here.
def index_Administrador(request):
    return render(request, 'Administrador/Index.html') 

def Administrador_view(request):
    if request.method == 'POST':
        form = AdministradorForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = AdministradorForm()

    return render(request, 'Administrador/Administrador_form.html', {'form': form})


def Administrador_list(request):
    administrador = cliente.objects.all().order_by('id')
    contexto = {'administrador': administrador}
    return render(request, 'Administrador/Administrador_list.html', contexto)


def Administrador_edit(request, id_administrador):
    administrador = cliente.objects.get(id=id_administrador)
    if request.method == 'GET':
        form = AdministradorForm(instance=administrador)
    else:
        form =AdministradorForm(request.POST, instance=administrador)
        if form.is_valid():
            form.save()
    return render(request, 'Administrador/Administrador_form.html', {'form': form})

def Administrador_delete(request, id_administrador):
    administrador = cliente.objects.get(id=id_administrador)
    if request.method == 'POST':
        administrador.delete()
    return render(request, 'Administrador/Administrador_delete.html', {'administrador': administrador})
