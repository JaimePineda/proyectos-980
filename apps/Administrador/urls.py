from django.conf.urls import url, include

from apps.Administrador.views import index_Administrador, Administrador_view, Administrador_list, Administrador_edit, Administrador_delete

urlpatterns = [
    url(r'^$', index_Administrador, name='index_Administrador'),
    url(r'^nuevo$', Administrador_view, name='Administrador_crear'),
    url(r'^lista$', Administrador_list, name='Administrador_list'),
    url(r'^editar/(?P<id_administrador>\d+)$', Administrador_edit, name='Administrador_edit'),
    url(r'^eliminar/(?P<id_administrador>\d+)$', Administrador_delete, name='Administrador_delete'),

]

# el simbolo de $ nos indica donde termina la cadena
# el simbolo de ^ nos indica donde comienza la cadena
 