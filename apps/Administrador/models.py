from django.db import models

# Create your models here.
class cliente(models.Model):
    nombre_completo = models.CharField(max_length=100)
    nombre_usuario = models.CharField(max_length=100)
    correo = models.EmailField()
    telefono = models.CharField(max_length=20)
    edad = models.IntegerField()
    fotografia = models.CharField(max_length=100)
    contrasenia = models.CharField(max_length=50)

    def __str__(self):
        return '{} {}'.format(self.nombre_completo, self.nombre_completo)