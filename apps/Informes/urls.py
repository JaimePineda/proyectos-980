from django.conf.urls import url, include

from apps.Informes.views import Informe_list, Informe_view

urlpatterns = [
    url(r'^informe$', Informe_view, name='informe_form'),
    url(r'^lista$', Informe_list, name='informe_list'),
]