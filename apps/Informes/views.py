from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.
from apps.Informes.forms import InformesForm
from apps.Informes.models import inform 

def Informe_view(request):
    if request.method == 'POST':
        form = InformesForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = InformesForm()

    return render(request, 'Informe/Informe_form.html', {'form': form})


def Informe_list(request):
    informe = inform.objects.all().order_by('id')
    contexto = {'informe': informe}
    return render(request, 'Informe/Informe_list.html', contexto)
