from django import forms


from apps.Informes.models import inform

class InformesForm(forms.ModelForm):

    class Meta:
        model = inform

        fields = [
            'no_visitas',
            'compras',
            'medicina_recetada',
            'pago_efectivo',
            'pago_tarjeta',

        ]

        labels = {
            'no_visitas': 'No_visitas',
            'compras': 'Compras',
            'medicina_recetada': 'Medicina_Recetada',
            'pago_efectivo': 'Pago_Efectivo',
            'pago_tarjeta': 'Pago_Tarjeta',

        }

        widgets = {
            'no_visitas': forms.TextInput(attrs={'class': 'form-control'}),
            'compras': forms.TextInput(attrs={'class': 'form-control'}),
            'medicina_recetada': forms.TextInput(attrs={'class': 'form-control'}),
            'pago_efectivo': forms.TextInput(attrs={'class': 'form-control'}),
            'pago_tarjeta': forms.TextInput(attrs={'class': 'form-control'}),
        }