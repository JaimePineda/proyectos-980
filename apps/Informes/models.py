from django.db import models

# Create your models here.
class inform(models.Model):
    no_visitas = models.IntegerField()
    compras = models.CharField(max_length=500)
    medicina_recetada = models.CharField(max_length=500)
    pago_efectivo = models.CharField(max_length=50)
    pago_tarjeta = models.CharField(max_length=50)