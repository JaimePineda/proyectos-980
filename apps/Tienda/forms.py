from django import forms

from apps.Tienda.models import tiend

class TiendaForm(forms.ModelForm):

    class Meta:
        model = tiend

        fields = [
            'juguetes',
            'comida',
            'medicina',

        ]

        labels = {
            'juguetes': 'Juguetes',
            'comida': 'Comida',
            'medicina': 'Medicina',

        }

        widgets = {
            'juguetes': forms.TextInput(attrs={'class': 'form-control'}),
            'comida': forms.TextInput(attrs={'class': 'form-control'}),
            'medicina': forms.TextInput(attrs={'class': 'form-control'}),
         }

