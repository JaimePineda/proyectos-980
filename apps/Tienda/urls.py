from django.conf.urls import url, include

from apps.Tienda.views import index_Tienda, Tienda_view, Juguetes_Tienda, Comida_Tienda, Medicina_Tienda, Inicio_Tienda, Juguetes_Inicio, Comida_Inicio, Medicina_Inicio, Inicio_cliente

urlpatterns = [
    url(r'^$', index_Tienda, name='index_Tienda'),
    url(r'^nuevo$', Tienda_view, name='Tienda_view'),
    url(r'^juguetes$', Juguetes_Tienda, name='juguetes_view'),
    url(r'^comida$', Comida_Tienda, name='comida_view'),
    url(r'^medicina$', Medicina_Tienda, name='medicina_view'),
    url(r'^juinicio$', Juguetes_Inicio, name='juguetes_inicio_view'),
    url(r'^coinicio$', Comida_Inicio, name='comida_inicio_view'),
    url(r'^meinicio$', Medicina_Inicio, name='medicina_inicio_view'),
    url(r'^iniciocli$', Inicio_cliente, name='cliente_inicio_view'),
]