from django.shortcuts import render, redirect
from django.http import HttpResponse

from apps.Tienda.forms import TiendaForm
# Create your views here.
def index_Tienda(request):
    return render(request, 'Tienda/Index.html') 

def Juguetes_Tienda(request):
    return render(request, 'Tienda/Juguetes_Tienda.html') 

def Comida_Tienda(request):
    return render(request, 'Tienda/Comida_Tienda.html') 

def Medicina_Tienda(request):
    return render(request, 'Tienda/Medicina_Tienda.html') 

def Juguetes_Inicio(request):
    return render(request, 'Tienda/Inicio_juguetes.html') 

def Comida_Inicio(request):
    return render(request, 'Tienda/Inicio_comida.html') 

def Medicina_Inicio(request):
    return render(request, 'Tienda/Inicio_Medicina.html') 

def Inicio_Tienda(request):
    return render(request, 'Tienda/Inicio.html') 

def Inicio_cliente(request):
    return render(request, 'Tienda/Inicio_clien.html') 


def Tienda_view(request):
    if request.method == 'POST':
        form = TiendaForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = TiendaForm()

    return render(request, 'Tienda/Tienda_form.html', {'form': form})