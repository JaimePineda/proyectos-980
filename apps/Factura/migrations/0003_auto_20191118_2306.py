# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2019-11-19 05:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Factura', '0002_auto_20191117_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fact',
            name='fecha',
            field=models.CharField(max_length=100),
        ),
    ]
