from django.conf.urls import url, include

from apps.Factura.views import index_Factura, Factura_view, Factura_list

urlpatterns = [
    url(r'^$', index_Factura, name='index_Factura'),
    url(r'^/nuevo$', Factura_view, name='Factura_crear'),
    url(r'^factu$', Factura_list, name='Factura_lista'),
]

# el simbolo de $ nos indica donde termina la cadena
# el simbolo de ^ nos indica donde comienza la cadena
 