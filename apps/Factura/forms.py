from django import forms

from apps.Factura.models import fact
class FacturaForm(forms.ModelForm):

    class Meta:
        model = fact

        fields = [
            'fecha',
            'clien',
            'tien',
            'masc',

]

        labels = {
            'fecha': 'Fecha',
            'clien': 'Cliente',
            'tien': 'Tienda',
            'masc': 'Otros',

        }

        widgets = {
            'fecha': forms.TextInput(attrs={'class': 'form-control'}),
            'clien': forms.Select(attrs={'class': 'form-control'}),
            'tien': forms.Select(attrs={'class': 'form-control'}),
            'masc': forms.Select(attrs={'class': 'form-control'}),
     }

