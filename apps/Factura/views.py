from django.shortcuts import render, redirect
from django.http import HttpResponse

from apps.Factura.forms import FacturaForm
from apps.Factura.models import fact

# Create your views here.
def index_Factura(request):
    return render(request, 'Factura/Index.html') 

def Factura_view(request):
    if request.method == 'POST':
        form = FacturaForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = FacturaForm()

    return render(request, 'Factura/Factura_form.html', {'form': form})

def Factura_list(request):
    factura = fact.objects.all().order_by('id')
    contexto = {'factura': factura}
    return render(request, 'Factura/Factura_list.html', contexto)