from django.db import models

# Create your models here.
from apps.Administrador.models import cliente

class Vacuna(models.Model):
    vacunas = models.CharField(max_length=50)
    citas_pasadas = models.CharField(max_length=100)
    citas_venideras = models.CharField(max_length=100)
    tipo_cita = models.CharField(max_length=50)
    
    def __str__(self):
        return '{}'.format(self.vacunas)


class Mascota(models.Model):#importante cuando se declara el nombre de la clase siempre se coloca en singular ejemplo Mascota
    #En esta parte se colocan todos los atributos que va a llevar nuestra tabla de mascota
    nombre = models.CharField(max_length=50)
    edad = models.IntegerField()
    comentario = models.CharField(max_length=500)
    fotografia = models.CharField(max_length=500)
    persona = models.ForeignKey(cliente,null= True,blank=True,on_delete=models.CASCADE)
    vacuna =models.ManyToManyField(Vacuna, blank=True)