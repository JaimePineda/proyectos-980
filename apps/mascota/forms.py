from django import forms

from apps.mascota.models import Mascota

class MascotaForm(forms.ModelForm):

    class Meta:
        model = Mascota

        fields = [
            'nombre',
            'edad',
            'comentario',
            'fotografia',
            'persona',
            'vacuna',
        ]

        labels = {
            'nombre': 'Nombre',
            'edad': 'Edad',
            'comentario': 'Comentario',
            'fotografia': 'Fotografia',
            'persona': 'Persona',
            'vacuna': 'Vacuna',

        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.TextInput(attrs={'class': 'form-control'}),
            'comentario': forms.TextInput(attrs={'class': 'form-control'}),
            'fotografia': forms.TextInput(attrs={'class': 'form-control'}),
            'persona': forms.Select(attrs={'class': 'form-control'}),
            'vacuna': forms.CheckboxSelectMultiple(),
        }
