from django.conf.urls import url, include

from apps.mascota.views import index_Mascota, Mascota_view, Mascota_list, Mascota_edit, Mascota_delete, cliente_Mascota

urlpatterns = [
    url(r'^$', Mascota_view, name='index_mascota'),
    url(r'^lista$', Mascota_list, name='mascota_list'),
    url(r'^editar/(?P<id_mascota>\d+)$', Mascota_edit, name='mascota_edit'),
    url(r'^eliminar/(?P<id_mascota>\d+)$', Mascota_delete, name='mascota_delete'),
    url(r'^listacliente$', cliente_Mascota, name='mascota_cliente'),

    #url(r'^nuevo$', Mascota_view, name='mascota_crear'),
]

# el simbolo de $ nos indica donde termina la cadena
# el simbolo de ^ nos indica donde comienza la cadena
 