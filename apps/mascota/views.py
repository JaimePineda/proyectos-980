from django.shortcuts import render, redirect
from django.http import HttpResponse

from apps.mascota.forms import MascotaForm
from apps.mascota.models import Mascota
# Create your views here.

def index_Mascota(request):
    if request.method == 'POST':
        form = MascotaForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = MascotaForm()

    return render(request, 'mascota/Index.html') 

def cliente_Mascota(request):
    return render(request, 'mascota/Mascota_cliente.html') 


def Mascota_view(request):
    if request.method == 'POST':
        form = MascotaForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('Inicio_Administrador')
    else:
        form = MascotaForm()

    return render(request, 'mascota/Mascota_form.html', {'form': form})


def Mascota_list(request):
    mascota = Mascota.objects.all().order_by('id')
    contexto = {'mascota': mascota}
    return render(request, 'mascota/Mascota_list.html', contexto)


def Mascota_edit(request, id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'GET':
        form = MascotaForm(instance=mascota)
    else:
        form =MascotaForm(request.POST, instance=mascota)
        if form.is_valid():
            form.save()
    return render(request, 'mascota/Mascota_form.html', {'form': form})

def Mascota_delete(request, id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'POST':
        mascota.delete()
    return render(request, 'mascota/Mascota_delete.html', {'mascota': mascota})

