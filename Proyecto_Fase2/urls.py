"""Proyecto_Fase2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login
#from apps.mascota.views import Home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^Administrador/', include('apps.Administrador.urls', namespace="Inicio_Administrador")),
    url(r'^Superusuario/', include('apps.superusuario.urls', namespace="Inicio_Superusuario")),
    #url(r'^Mascota/', include('apps.mascota.urls', namespace="Inicio_Mascota")),  
    url(r'^Tienda/', include('apps.Tienda.urls', namespace='Inicio_Tienda')), 
    url(r'^Factura/', include('apps.Factura.urls', namespace='Inicio_Factura')),  
    url(r'^mascota/', include('apps.mascota.urls', namespace='Inicio_Mascota')), 
    url(r'^Informe/', include('apps.Informes.urls', namespace='Inicio_Informe')),     
    url(r'^$', login, {'template_name':'index.html'}, name='login')  

    # 
    # url(r'^', include('apps.mascota.urls', namespace="mascota")),
    #url(r'^home_mascota/', Home, name="index_mascota"),
    #url(r'^Administrador/', include('apps.Administrador.urls', namespace="Administrador")),
]
